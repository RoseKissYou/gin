package main

import (
	"fmt"
	"net/http"
)

func sayHello(w http.ResponseWriter, r *http.Request) {
	// 直接显示内容在页面上
	// _, _ = fmt.Println(w, "hello") // 这个不能显示在界面上
	_, _ = fmt.Fprintln(w, "Hello anxiong")
	// 从文件读取信息写到页面上,可以直接编辑样式
}

func main() {
	http.HandleFunc("/hello", sayHello)
	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Println("错误 %v\n", err)
		return
	}
}
