package main

import (
	"io"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	// 2021/1/16
	//  禁用控制台颜色
	gin.DisableConsoleColor()

	f, _ := os.Create("gin.log")

	gin.DefaultWriter = io.MultiWriter(f)

	router := gin.Default()
	router.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})
	router.Run(":9321")
}
