package main

import (
	"database/sql"
	"fmt"

	// 引入数据库驱动注册及初始化
	_ "github.com/go-sql-driver/mysql"
)

func main() {
	// sql.Register("mysql", &MySQLDriver{})

	db, err := sql.Open("mysql", "root:123456@tcp(192.168.1.2:3306)/test?charset=utf8")
	if err != nil {
		fmt.Println("failed to open database:", err.Error())
		return
	}
	fmt.Println("链接成功")
	defer db.Close()
}

// 参考实例代码
func demo(db *sql.DB) {
	// 获取USERS表中的前十行记录
	rows, err := db.Query("SELECT * FROM user")
	if err != nil {
		fmt.Println("fetech data failed:", err.Error())
		return
	}
	defer rows.Close()
	for rows.Next() {
		var uid int
		var name, password string
		rows.Scan(&uid, &name, &password)
		fmt.Println("uid:", uid, "name:", name, "password:", password)
	}
}
