package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	// 2021/1/16
	fmt.Println("hello")
	// 1.创建路由
	r := gin.Default()
	//限制上传最大尺寸 8M
	r.MaxMultipartMemory = 8 << 20
	// 2.绑定路由规则，执行的函数
	// gin.Context，封装了request和response
	r.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "hello World!")
	})
	r.POST("/upload", func(c *gin.Context) {
		fmt.Println("上传文件")
		file, err := c.FormFile("file")
		if err != nil {
			c.String(500, "上传图片出错")
		}
		fmt.Println("上传文件", err)
		// c.JSON(200, gin.H{"message": file.Header.Context})
		c.SaveUploadedFile(file, file.Filename)
		c.String(http.StatusOK, file.Filename)
	})
	// 3.监听端口，默认在8080
	// Run("里面不指定端口号默认为8080")
	r.Run(":9580")
}
