package db

import (
	"database/sql"
	"fmt"
)

func Hello() {
	fmt.Println("hello")
}

func ConnectMysql() *sql.DB {
	mysqlDb, err := sql.Open("mysql", "root:123456@tcp(127.0.0.1:3306)/abell?charset=utf8")
	if err != nil {
		fmt.Println("failed to open database:", err.Error())
	}
	fmt.Println("链接成功")
	return mysqlDb
}
