package db

import (
	"database/sql"
	"fmt"
)

func InsertData(rawSql string, db *sql.DB) {
	fmt.Println("运行sql", rawSql)
	result, err := db.Exec(rawSql)
	if err != nil {
		fmt.Println("插入数据错误", err.Error())
		return
	}

	id, err := result.LastInsertId()
	if err != nil {
		fmt.Println("插入失败", err.Error())
	}
	fmt.Println("插入新数据", id)
}
