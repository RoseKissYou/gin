package main

import (
	"fmt"
	"gin/1insert/db"

	//"main/insert/db"
	"math/rand"

	// 引入数据库驱动注册及初始化
	_ "github.com/go-sql-driver/mysql"
)

func main() {
	//fmt.Println("回来了")
	fmt.Println(getRandName(rand.Intn(3), true))
	fmt.Println(getRandName(rand.Intn(3), false))
}

func getRandName(num int, isZh bool) string {
	var str string
	num = num + 2 // 至少有2个字符长度
	if isZh {
		str = "一乙二十丁厂七卜人入八九几儿了力乃刀又三于干亏士工土才寸下大丈与万上小口巾山千乞川亿个勺久凡及夕丸么广亡门义之尸弓己已子卫也女飞刃习叉马乡丰王井开夫天无元专云扎艺木五支厅不太犬区历尤友匹车巨牙屯比互切瓦止少日中冈贝内水见午牛手毛气升长仁什片仆化仇币仍仅斤爪反介父从今凶分乏公仓月氏勿欠风丹匀乌凤勾文六方火为斗忆订计户认心尺引丑巴孔队办以允予劝双书幻玉刊示末未击打巧正扑扒功扔去甘世古节本术可丙左厉右石布龙平灭轧东卡北占业旧帅归且旦目叶甲申叮电号田由史只央兄叼叫另叨叹四生失禾丘付仗代仙们仪白仔他斥瓜乎丛令用甩印乐句匆册犯外处冬鸟务包饥主市立闪兰半汁汇头汉宁穴它讨写让礼训必议讯记永司尼民出辽奶奴加召皮边发孕圣对台矛纠母幼丝式刑动扛寺吉扣考托老执巩圾扩扫地扬场耳共芒亚芝朽朴机权过臣再协西压厌在有百存而页匠夸夺灰达列死成夹轨邪划迈毕至此贞师尘尖劣光当早吐吓虫曲团同吊吃因吸吗屿帆岁回岂刚则肉网年朱先丢舌竹迁乔伟传乒乓休伍伏优伐延件任伤价份华仰仿伙伪自血向似后行舟全会杀合兆企众爷伞创肌朵杂危旬旨负各名多争色壮冲冰庄庆亦刘齐交次衣产决充妄闭问闯羊并关米灯州汗污江池汤忙兴宇守宅字安讲军许论农讽设访寻那迅尽导异孙阵阳收阶阴防奸如妇好她妈戏羽观欢买红纤级约纪驰巡寿弄麦形进戒吞远违运扶抚坛技坏扰拒找批扯址走抄"
	} else {
		str = "1234567890qwertyuiopasdfghjklzxcvbnm"
	}
	rt := []rune(str)
	getName := ""
	for i := 0; i < num; i++ {
		getName = getName + string(rt[rand.Intn(len(rt)-1)])
	}
	return getName
}

func getRandData(num int, startNum int) string {
	var resultSql string
	resultSql = "INSERT INTO `person`(`pid`, `pname`, `psex`, `page`, `sal`) VALUES "
	for i := startNum; i < num; i++ {
		var randSexName string
		if rand.Intn(2) > 0 {
			randSexName = "男"
		} else {
			randSexName = "女"
		}
		resultSql += fmt.Sprintf("(%d, 'hello%d', '%s', %d, %d),", i, i, randSexName, i, rand.Intn(9999))
	}
	return resultSql[:len(resultSql)-1] + ";"
}

func insertData() {
	mysqlDb := db.ConnectMysql()
	// 随机生成用户名
	insertRawSql := "INSERT INTO `person`(`pid`, `pname`, `psex`, `page`, `sal`) VALUES (5, 'hello', '男', 1, 10.00);"
	db.InsertData(insertRawSql, mysqlDb)
	defer mysqlDb.Close()
}

// 一次次插入数据db
func insertDataOneByOne() {
	mysqlDb := db.ConnectMysql()
	startNum := 200
	num := 9999999
	for i := startNum; i < num; i++ {
		var randSexName string
		if rand.Intn(2) > 0 {
			randSexName = "男"
		} else {
			randSexName = "女"
		}
		querySql := fmt.Sprintf("INSERT INTO `person`(`pid`, `pname`, `psex`, `page`, `sal`) VALUES (%d, 'hello%d', '%s', %d, %d);", i, i, randSexName, i, rand.Intn(9999))
		fmt.Println(querySql)
		db.InsertData(querySql, mysqlDb)
	}
	defer mysqlDb.Close()
}
