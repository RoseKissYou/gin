package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	// 2021/1/16
	r := gin.Default()

	v1 := r.Group("/v1")
	{
		v1.GET("/login", login)
		v1.GET("/reg", reg)
		v1.GET("/json", showJson)
		v1.GET("/info", showInfo)
	}

	r.Run(":9580")
}

func login(c *gin.Context) {
	name := c.DefaultQuery("name", "jack")
	c.String(http.StatusOK, fmt.Sprintf("HELLO login ok %s \n", name))
}

func reg(c *gin.Context) {
	name := c.DefaultQuery("name", "hello")
	c.String(http.StatusOK, fmt.Sprintf("hello reg ok %s \n", name))
}

func showInfo(c *gin.Context) {
	id := c.Query("id")
	page := c.DefaultQuery("page", "10")
	c.String(http.StatusOK, fmt.Sprintf("HELLO id = %s ; page = %s \n", id, page))
	// fmt.Printf("show shome id = %s; page = %s \n", id, page)
}

func showJson(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"errCode": 0,
		"msg":     "ok",
	})
}
