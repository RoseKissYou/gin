package main

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/bson"
)

func main() {
	client, err := connect()
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(context.Background())

	// 插入文档
	doc := bson.M{"username": "testuser", "email": "testuser@example.com", "age": 25}
	err = insertDocument(client, "mydb", "users", doc)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("插入文档成功")

	// 查询文档
	filter := bson.M{"username": "testuser"}
	docs, err := findDocuments(client, "mydb", "users", filter)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("查询结果：", docs)

	// 更新文档
	update := bson.M{"email": "updated_email@example.com", "age": 26}
	err = updateDocument(client, "mydb", "users", filter, update)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("更新文档成功")

	// 查询更新后的文档
	docs, err = findDocuments(client, "mydb", "users", filter)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("更新后的查询结果：", docs)

	// 删除文档
	err = deleteDocuments(client, "mydb", "users", filter)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("删除文档成功")

	// 查询删除后的文档
	docs, err = findDocuments(client, "mydb", "users", filter)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("删除后的查询结果：", docs)
}
