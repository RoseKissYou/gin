package main

import "fmt"

func SayHi(name string) string {
	return fmt.Sprintf("Hi, %s", name)
}

func main() {
	fmt.Println("HEllo")

	hello := SayHi("Hello")
	fmt.Println(hello)
}
