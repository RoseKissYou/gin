package main

import (
	"fmt"
	"io"
	"os"
	"time"

	"github.com/gin-gonic/gin"
)

func main() {
	// 2021/1/16
	router := gin.New()

	router.Use(gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {

		// 你的自定义格式
		return fmt.Sprintf("%s - [%s] \"%s %s %s %d %s \"%s\" %s\"\n",
			param.ClientIP,
			param.TimeStamp.Format(time.RFC1123),
			param.Method,
			param.Path,
			param.Request.Proto,
			param.StatusCode,
			param.Latency,
			param.Request.UserAgent(),
			param.ErrorMessage,
		)
	}))
	router.Use(gin.Recovery())

	// 写入文件

	f, _ := os.Create("newlog.log")

	gin.DefaultWriter = io.MultiWriter(f)

	router.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})
	router.Run(":9321")
	// 访问 http://127.0.0.1:9321/ping 可以看到返回值
}
