package main

import (
	"fmt"
	"net"
	"os"
	"strings"
	"sync"
)

type User struct {
	UserName      string
	OtherUserName string
	Msg           string
	ServerMsg     string
}

var (
	user = new(User)
	wg   sync.WaitGroup
)

func main() {
	wg.Add(1)
	fmt.Println("请输入账号")
	fmt.Scanln(&user.UserName)
	fmt.Println("请输入要给谁发送消息：")
	fmt.Scanln(&user.OtherUserName)
	addr, _ := net.ResolveTCPAddr("tcp4", "localhost:8899")
	conn, _ := net.DialTCP("tcp4", nil, addr)
	// sendMsg 发送消息
	go func() {
		fmt.Println("请输入您要发送的消息： ")
		for {
			fmt.Scanln(&user.Msg)
			if user.Msg == "exit" {
				conn.Close()
				wg.Done()
				os.Exit(0)
			}
			conn.Write([]byte(fmt.Sprintf("%s-%s-%s-%s", user.UserName, user.OtherUserName, user.Msg, user.ServerMsg)))
		}
	}()
	// 接受消息
	go func() {
		for {
			b := make([]byte, 1024)
			count, _ := conn.Read(b)
			array := strings.Split(string(b[:count]), "-")
			user2 := new(User)
			user2.UserName = array[0]
			user2.OtherUserName = array[1]
			user2.Msg = array[2]
			user2.ServerMsg = array[3]
			if user2.ServerMsg != "" {
				fmt.Println("\t\t服务器的消息：", user2.ServerMsg)
			} else {
				fmt.Println("\t\t", user2.UserName, ":", user2.Msg)
			}
		}

	}()
	wg.Wait()
}
