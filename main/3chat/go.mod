module chat

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/helloh2o/lucky v1.0.5 // indirect
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
)
