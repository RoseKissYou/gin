package main

import (
	"fmt"
	"net"
)

func main() {
	// 创建服务器地址
	addr, _ := net.ResolveTCPAddr("tcp4", "localhost:8089")
	// 创建监听器
	tcp, _ := net.ListenTCP("tcp4", addr)
	fmt.Println("启动")
	// 获取数据
	// 关闭连接
	for {
		accept, _ := tcp.Accept()
		go func() {
			bytes := make([]byte, 1024)
			n, _ := accept.Read(bytes)
			fmt.Println("获取到的数据为：", string(bytes[:n]))
			accept.Write(append([]byte("服务器返回来的消息:"), bytes[:n]...))
			accept.Close()
		}()
	}
}
