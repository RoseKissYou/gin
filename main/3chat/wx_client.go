package main

import (
	"fmt"
	"net"
)

func main() {
	addr, _ := net.ResolveTCPAddr("tcp4", "localhost:8089")

	conn, _ := net.DialTCP("tcp4", nil, addr)
	conn.Write([]byte("客户端发送数据   hello"))
	b := make([]byte, 1024)
	count, _ := conn.Read(b)
	fmt.Println("server:", string(b[:count]))
	conn.Close()

}
