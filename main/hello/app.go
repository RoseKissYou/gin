package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"reflect"
	"time"
)

// 进入项目  go mod tidy 使用当前目录和环境加载扩展  运行  go run app.go
func main() {

	fmt.Println("hello")
	name := "hello"
	nowTime := time.Now().Unix()
	fmt.Println(nowTime)
	fmt.Println(name)
	fmt.Println(reflect.TypeOf(nowTime))
	fmt.Printf("hello%d\n", nowTime)

	// pass := "sn20170529"
	// pass = "fly20180717"
	// md5_2(pass)
}

func gin() {
	// 1.创建路由
	// r := gin.Default()
	// // 2.绑定路由规则，执行的函数
	// // gin.Context，封装了request和response
	// r.GET("/", func(c *gin.Context) {
	// 	c.String(http.StatusOK, "hello World!")
	// })
	// // 3.监听端口，默认在8080
	// r.Run(":9580")
}

func md5_1(s string) {
	m := md5.New()
	m.Write([]byte(s))
	fmt.Println(hex.EncodeToString(m.Sum(nil)))
}

func md5_2(s string) {
	m := md5.Sum([]byte(s))
	fmt.Println(hex.EncodeToString(m[:]))
}

func md5_3(s string) {
	m := md5.Sum([]byte(s))
	fmt.Printf("%x", m)
	fmt.Println()
}
