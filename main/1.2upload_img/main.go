package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.POST("/upload", func(c *gin.Context) {
		file, fileHeader, _ := c.Request.FormFile("file")
		fmt.Println(file)
		fmt.Println(fileHeader.Size)
		fmt.Println(fileHeader.Filename)

		//log.Println(file.Filename)

		// Upload the file to specific destination.
		dst := fileHeader.Filename
		err := c.SaveUploadedFile(fileHeader, dst)
		if err != nil {
			fmt.Println("保存文件失败", err)
			return
		}
		//
		c.String(http.StatusOK, fmt.Sprintf("'%s' uploaded!", fileHeader.Filename))
	})
	r.Run(":3000")
}
