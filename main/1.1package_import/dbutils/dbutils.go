package dbutils

import "fmt"

// 这里方法的首字母小写不能被其他包访问,这里首字母改成大写就可以被外界访问了
func getConn() {
	fmt.Println("执行 util 里面的 getConn包")
}

func PublicGetConn() {
	fmt.Println("可以被外界访问的 util 的 getCon 方法")
}
