package main

import (
	"fmt"
	"main/calutils" //包名和文件夹名称可以不一致,最终以包名为准,而且需要在src文件下可以找到
	"main/dbutils"
)

const PI = 3.14159

type Circle struct {
	radius float64
}

// 声明一个方法,计算圆的面积,这里使用Circle方法的对象,但是通常是传入地址的
func (c Circle) area() float64 {
	return PI * c.radius * c.radius
}

// 正常的写法是这样的,而且这里编译器会直接获取 (&c).radius 进行计算
func (c *Circle) area2() float64 {
	return PI * (*c).radius * (*c).radius
}

// 实际上可以不需要加*就已经可以取到地址的内容了
func (c *Circle) area3() float64 {
	return PI * c.radius * c.radius
}

// 引入其他包的包名是从 $GOPATH/src 后开始计算的,使用 / 进行路径分隔
// 查看 $GOPATH 可以通过 go env 查看,这里看到路径是 GOPATH='/Users/anxiong/go'

// 调用外部方法
func getPublicFunc() {
	// 处理调用外部方法
	dbutils.PublicGetConn()
	dbutils.PublicModel()

	calutils.CalTool() // 不在 src 文件下,还是要保持文件名和包名一致,方便调用
}

func main() {
	fmt.Println("计算 圆")
	var c Circle
	// c.radius = 4.0
	// res := c.area()
	// fmt.Println("面积是=", res)

	// 标准的使用 & 写法
	// c.radius = 6.0
	// res2 := (&c).area2()
	// fmt.Println("标准加*写法,面积是=", res2)

	// 这里不加取地址也可以运行而且的编译器支持的
	c.radius = 7.0
	res3 := c.area3()
	fmt.Println("不加*写法,面积是=", res3)

}
