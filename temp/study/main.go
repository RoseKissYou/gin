package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Println("hello")
	pointStudy()
}

// 指针
func pointStudy() {
	fmt.Println("指针计算")

	var age int
	fmt.Println(&age) // 0xc0000960ba
	age = 19

	// *int 表示整数指针,这里必须存 int 类型数据的地址
	var ptr *int = &age
	fmt.Println(ptr)  // 0xc00000a120
	fmt.Println(*ptr) // 这里用 * 取地址对应的值,就可以获取到 19
	*ptr = 20
	fmt.Println("*ptr=", *ptr, "age=", age) // *ptr= 20 age= 20
	// 这里可以看到指针就是引用传递,修改引用的参数实际上就会修改原值

}

// 数据格式转换和进制变换
func numFormat() {
	var num1 float32 = 314e-2
	fmt.Println(num1)
	var num3 int8
	num3 = 127
	fmt.Println(num3)
	num4 := num3 + 2
	fmt.Println(num4)
	kitty := fmt.Sprintf("fadfasd == %T", num4)
	hello := strconv.FormatInt(int64(num3), 8) // 进制转换 前面的int64数据,后面的是进制,这里表示八进制
	fmt.Println(hello)
	fmt.Println(kitty)
}
