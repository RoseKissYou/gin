

# 在 MF839 上安装go 2023年10月12日
```shell
brew install go
# 在更新了 brew 的版本以后安装成功了
% go version
go version go1.21.3 darwin/amd64
# 可以看到安装成功的版本 

```
# 切换国内源
```shell
go env -w GO111MODULE=on
go env -w GOPROXY=https://goproxy.cn

# 执行上面的命令就可以实现切换成为国内源了



```



