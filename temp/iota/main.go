package main

import "fmt"

const (
	n1 = iota
	n2
	n3
)

func main() {
	fmt.Printf("Hello")
	fmt.Println(n1) // 默认是 0
	fmt.Println(n2) // 1
	fmt.Println(n3) // 2
}
