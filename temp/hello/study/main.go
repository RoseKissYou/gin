package main

import (
	"fmt"
	connectMysql "main/db"
	"main/show"
	"main/utils"
)

func test(num int) {
	fmt.Println(num)
}

func main() {
	fmt.Println("kitty")
	show.HelloKitty() // 10
}

func someReq() {
	// defer
	result := show.UseDefer(19, 2)
	fmt.Println(result)

	// 匿名函数
	show.NoNameFunction()

	// 闭包的定义: 就是一个函数和其他相关的引用环境组合的一个整体
	// 闭包的引用举例: 返回的匿名函数 + 匿名函数以外的变量 sum
	g := show.GetSum()
	// GetSum 参数为空,但是返回值是一个函数,这个函数的参数是int类型的参数,返回值也是int类型的
	// 这里初始化这个函数,然后给这个函数传参数也可以得到返回值
	fmt.Println(g(1)) // 1
	fmt.Println(g(2)) // 3 这里把上次的结果作为起始值进行处理,这个sum参数其实是保存在 g 这个变量里面的
	fmt.Println(g(3)) // 6 这里同理,保存了上面计算结果 sum = 4,这里计算 4 + 2 =6
	fmt.Println(g(4))

	// 但是直接使用传值调用方法,这里每次都是从0开始累加,所以下面的数据都是1
	h := show.GetFuncSum(1)
	fmt.Println(h)
	fmt.Println(h)
	fmt.Println(h)

	// 也可以使用方法传值实现类似闭包的效果
	m := show.GetAddSum(0, 1) // 这里需要把上次结算的结果传入到当前的第一个参数,进行累加操作
	fmt.Println(m)
	n := show.GetAddSum(1, 2) // 3
	fmt.Println(n)
	n = show.GetAddSum(3, 3) // 6
	fmt.Println(n)
	// 通过这里可以看到闭包和普通方法的用处,可以存储一些参数和其他环境一起调用,保留上次计算的数据

	show.FuncDetail()
	a := test
	fmt.Printf("a 方法类型 %T \n", a) // a 方法类型 func(int)

	hello := show.HelloTencent // 这里把方法赋值给变量
	hello(1, "kitty")          // 通过变量带参数来调用,这个时候变量就成了方法的使用
	// fmt.Println(hello)

	// show.GetShow()
	// show.CalFor()

	// result := show.SumTowNum(10, 8) // 调用函数
	// fmt.Println(result)
	// show.ArgsMethod(1, 3, 5, 7) // 函数穿可变参数作为切片处理
	// show.MyType
	sum := utils.AddSome(1, 5)
	fmt.Println(sum)
	// 给调用的包起别名
	connectMysql.GetConnect()

	// 在包里面初始化数据,这里直接调用
	utils.InitData()
	fmt.Println(utils.Age)
}

// 指针定义学习
func ptrStudy() {
	fmt.Println("学习指针")
	// 声明一个变量
	var num int
	num = 8
	fmt.Println(num) // 显示变量的值 8
	// 声明一个int类型的指针变量,这里存储int值的指针地址
	var ptr *int
	ptr = &num // 把 num 的指针地址存储到 ptr
	fmt.Println(ptr)
	fmt.Println(&num)
	// 改变 ptr 的值
	*ptr = 6
	fmt.Println(num) // 这里 num 被改成了6,通过指针变量修改了num,也就是引用传递
	// fmt.Println(&num)
}
