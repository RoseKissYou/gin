package show

import "fmt"

func GetShow() {
	fmt.Println("hello")
}

// for 循环
func CalFor() {
	for i := 1; i < 10; i++ {
		// fmt.Println("hello" + string(i))
		fmt.Printf("当前操作 %d \n", i)
	}
	// %s	直接输出字符串或者[]byte
	// %q	该值对应的双引号括起来的go语法字符串字面值，必要时会采用安全的转义表示
	// %x	每个字节用两字符十六进制数表示（使用a-f）
	// %X	每个字节用两字符十六进制数表示（使用A-F）
	longStr := "hellokitty"
	for aIndex, aStr := range longStr {
		fmt.Printf("循环遍历 %d, %q \n", aIndex, aStr)
	}
}

// 求和,返回一个int参数
func SumTowNum(numA int, numB int) int {
	return numA + numB
}

// 闭包
// GetSum 参数为空,但是返回值是一个函数,这个函数的参数是int类型的参数,返回值也是int类型的
func GetSum() func(int) int {
	var sum int = 0
	return func(i int) int {
		sum = sum + i
		return sum
	}
}

// 与闭包相对应的直接调用方法
func GetFuncSum(num int) int {
	var sum int = 0
	sum = sum + num
	return sum
}

// 用普通方法实现和闭包类似的效果
func GetAddSum(sum int, num int) int {
	sum = sum + num
	return sum
}

// 使用 defer 关键词,也就是延后执行语句
func UseDefer(num1 int, num2 int) int {
	// 这里程序遇到 defer 不会立即运行 defer 后面的语句
	defer fmt.Println("num1 = ", num1) // 执行顺序 2
	defer fmt.Println("num2 = ", num2) // 执行顺序 3
	// 栈的特点是先进后出
	// 在函数执行完以后再从栈里面取出数据进行运行
	var sum int = num1 + num2
	fmt.Println("sum=", sum) // 执行顺序 1
	return sum
}
