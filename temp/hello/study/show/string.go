package show

import "fmt"

func HelloKitty() {
	hello := "hellokitty"
	fmt.Println(len(hello))

	// for i := 0; i < len(hello); i++ {
	// 	// fmt.Println(hello[i]) // 这里遍历字符会转换为 askII 码进行处理
	// 	// 想要显示原始的字符的话需要使用转义显示
	// 	fmt.Printf("查看字符 %c \n", hello[i])
	// }

	for i, v := range hello {
		fmt.Println(i)
		// fmt.Println(v)
		fmt.Printf("查看字符 %c \n", v)
	}
}
