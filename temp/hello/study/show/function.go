package show

import "fmt"

// func ArgsMethod(args ...int)
// show/function.go:8:6: missing function body
// 这里定义了函数,但是没有body会报错提示

// 函数内部处理可变参数
func ArgsMethod(args ...int) {
	fmt.Println("hello args")
	// 这里函数内部处理可变参数的时候,将可变参数当做切片来处理
	for i := 0; i < len(args); i++ {
		// 这里通过for循环把args可变参数的所有参数拿出来显示处理
		fmt.Println(args[i])
	}
}

// 函数的细节
func FuncDetail() {
	fmt.Println("hello function")
}

// 方法A
func HelloTencent(num int, aStr string) {
	fmt.Println(num, aStr)
}

// 自定义数据类型
func MyType() {
	type myInt int
	var num myInt
	num = 8
	fmt.Println(num)

	var num2 int
	// num2 = num // 这里自定义的int类型,识别的时候认为这两个类型不一样,不可以直接赋值
	// ./main.go:21:9: cannot use num (variable of type myInt) as int value in assignment
	num2 = int(num) // 但是在数据类型转换后就可以赋值了
	fmt.Println(num2)
}

// 匿名函数的定义和使用
func NoNameFunction() {
	result := func(num1 int, num2 int) int {
		return num1 + num2
	}(10, 20)

	fmt.Println(result)

}
