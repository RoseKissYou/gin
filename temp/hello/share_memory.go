import "sync"

var mp sync.Map

// 学习关于go 的内存共享,并发读写的情况 https://www.bilibili.com/video/BV1EL4y1P7hh/?p=2&spm_id_from=pageDriver&vd_source=0edc6e41570cd7a1d7dae6d34d5bd0b3
func rwGlobleMemory(){
	value,exist := mp.load("mykey");exist{
		fmt.Println(value)
	} else {
		mp.Store("mykey", "myvalue")
	}
}

func main(){
	go rwGlobleMemory()
	go rwGlobleMemory()
	go rwGlobleMemory()
	go rwGlobleMemory()

	time.Sleep(time.Second)
}