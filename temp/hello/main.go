package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

var timeLayoutStr = "2006-01-02 15:04:05" //go中的时间格式化必须是这个时间
var timeLayoutStr2 = "2006/1/2 15:04:05"  //格式可以改变,但必须是这个时间

func main() {
	fmt.Println("hello gin")
	ManyRouterRequest()
}

// ManyRouterRequest 2 多路由请求 https://www.kancloud.cn/shuangdeyu/gin_book/949415
func ManyRouterRequest() {
	// 使用默认中间件创建一个gin路由器
	// logger and recovery (crash-free) 中间件
	router := gin.Default()

	router.GET("/getInfo", GetInfo)
	router.GET("/", HomeWeb)
	router.GET("/getParams/:name", GetWithParams)
	router.GET("/getParam2/:name/*action", GetParamsByKey)
	router.GET("/getdatabyquery", GetDataByQuery)
	router.GET("/getmessage", GetMessage)
	router.POST("/somePost", posting)
	// router.PUT("/somePut", putting)
	// router.DELETE("/someDelete", deleting)
	// router.PATCH("/somePatch", patching)
	// router.HEAD("/someHead", head)
	// router.OPTIONS("/someOptions", options)

	// 默认启动的是 8080端口，也可以自己定义启动端口
	router.Run(":9501")
	// router.Run(":3000") for a hard coded port
}

// GetMessage http://127.0.0.1:9501/getmessage?name=kitty
func GetMessage(c *gin.Context) {
	name := c.DefaultQuery("name", "hello")
	c.String(http.StatusOK, name)
}

// GetDataByQuery ---------- 2.3.1 获取get请求参数
// http://127.0.0.1:9501/getdatabyquery?firstname=hello&lastname=kitty
// Hello hello kitty
func GetDataByQuery(c *gin.Context) {
	firstname := c.DefaultQuery("firstname", "Guest")
	lastname := c.Query("lastname") // 是 c.Request.URL.Query().Get("lastname") 的简写
	c.String(http.StatusOK, "Hello %s %s", firstname, lastname)
}

// GetParamsByKey ---------- 2.2.2 通过路径里面获取参数
// http://127.0.0.1:9501/getParam2/john/send
// 返回的结果 john is /send 貌似*action 多带了一个 /
func GetParamsByKey(c *gin.Context) {
	name := c.Param("name")
	action := c.Param("action")
	message := name + " is " + action
	c.String(http.StatusOK, message)
}

// GetWithParams ---------- 2.2.1 获取路径里面参数的get请求
// http://127.0.0.1:9501/getParams/abell
// 返回数据 Hello abell
func GetWithParams(c *gin.Context) {
	name := c.Param("name")
	c.String(http.StatusOK, "Hello %s", name)
}

// SimpleRequest 1 基础请求
// 请求  http://127.0.0.1:8080/ping
// 返回json  {"message":"pong"}
func SimpleRequest() {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	r.Run() // listen and serve on 0.0.0.0:8080
}

// GetInfo ---------- 2.1 多路由请求
// http://127.0.0.1:9501/someGet
// 返回数据 {"message":"ok","status":0}
func GetInfo(c *gin.Context) {
	c.JSON(200, gin.H{
		"status":  0,
		"message": "ok",
	})
}

func HomeWeb(c *gin.Context) {
	c.JSON(200, gin.H{
		"status":  0,
		"message": "home web",
	})
}

// http://passport.test.tmeoa.com/authorize?appkey=2d7482d9f1450b996e01208d43f0bc3c&redirect_uri=http%3A%2F%2F127.0.0.1%3A8888%2Ftpp%2Flogin&state=STATE

type result struct {
	Args    string            `json:"args"`
	Headers map[string]string `json:"headers"`
	Origin  string            `json:"origin"`
	Url     string            `json:"url"`
}

// GetJsonFromUri 解析json类型返回结果
func GetJsonFromUri() {
	resp, err := http.Get("https://api.wrdan.com/hitokoto")
	if err != nil {
		return
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body))
	var res result
	_ = json.Unmarshal(body, &res)
	fmt.Printf("%#v", res)
}

// GetQQweb -------------发起get请求获取web页面数据
func GetQQweb() {
	resp, err := http.Get("https://api.wrdan.com/hitokoto")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body))
	fmt.Println(resp.StatusCode)
	if resp.StatusCode == 200 {
		fmt.Println("ok")
	}
}

// ---------------------------------------------------------------- 其他小插件 ----------------------------------------------------------------
func showTime() {
	ts := time.Now().Format(timeLayoutStr2)
	fmt.Println("获取当前时间")
	fmt.Println(ts)
}

func listTimes() {
	// 当前系统时间 2020-03-25 16:49:52.263362 +0800 CST m=+0.000856804
	fmt.Println(time.Now())
	// 档期时间戳 1585126192
	fmt.Println(time.Now().Unix())
	// 同time.Now() 2020-03-25 16:49:52.263362 +0800 CST m=+0.000856804
	fmt.Println(time.Now().String())
	// 年月日 （2020 March 25）
	fmt.Println(time.Now().Date())
	// 年
	fmt.Println(time.Now().Year())
	// 月
	fmt.Println(time.Now().Month())
	// 日
	fmt.Println(time.Now().Day())
	// 时
	fmt.Println(time.Now().Hour())
	// 分
	fmt.Println(time.Now().Minute())
	// 秒
	fmt.Println(time.Now().Second())
	// 当前时间 （yyyy-mm-dd h:i:s）
	fmt.Println(time.Unix(time.Now().Unix(), 0).Format("2006-01-02 15:04:05"))
}
